{ pkgs ? import <nixpkgs> {}
, lib ? pkgs.lib
, firefox-unwrapped ? pkgs.firefox-unwrapped

# General configuration
, extraPolicies ? {}
, extraPrefs ? ""
, extraCliArgs ? ""

, extensions ? []

# Profile setup
, readOnly ? false
, makePersistent ? []
, extraPersistentData ? []
, makeReadOnly ? []
, extraReadOnlyData ? []
, profileLocation ? null
, profileName ? "profile"
# Wait for other processes to finish syncing at startup
, linearSync ? true

# Misc stuff
, exeName ? "firefox"
, wmClass ? "Firefox-With"
, allowRemoteCommands ? false
, privateHome ? true
, debug ? false
, checkJs ? true
, preSandbox ? ""
, fuse-overlayfs ? pkgs.fuse-overlayfs
}:
assert lib.versionAtLeast (lib.getVersion fuse-overlayfs) "0.7";
let
  overlay = "${pkgs.fuse-overlayfs}/bin/fuse-overlayfs";
  mount = "${pkgs.utillinux}/bin/mount";
  mkdir = "${pkgs.coreutils}/bin/mkdir";
  touch = "${pkgs.coreutils}/bin/touch";
  cp = "${pkgs.coreutils}/bin/cp";
  rm = "${pkgs.coreutils}/bin/rm";
  mv = "${pkgs.coreutils}/bin/mv";
  uuidgen = "${pkgs.utillinux}/bin/uuidgen";
  sed = "${pkgs.gnused}/bin/sed";
  basename = "${pkgs.coreutils}/bin/basename";
  readlink = "${pkgs.coreutils}/bin/readlink";
  ln = "${pkgs.coreutils}/bin/ln";
  ps = "${pkgs.procps}/bin/ps";
  timeout = "${pkgs.coreutils}/bin/timeout";
  tail = "${pkgs.coreutils}/bin/tail";
  sponge = "${pkgs.moreutils}/bin/sponge";
  fusermount = "${pkgs.fuse}/bin/fusermount";

  log = s: if debug then ''echo "[firefox-with]" "${s}"'' else "true";
  err = s: ''echo "[firefox-with] error:" "${s}"'';

#-----------------CONFIGURATION-----------------
  enterprisePolicies = {
    policies = extraPolicies // {
      ExtensionSettings = with lib; fold (a: b: a // b) {} (
        map (ext: lib.optionalAttrs (ext ? "id" && ext ? "url") {
          "${getAttr "id" ext}" = {
            install_url = getAttr "url" ext;
            installation_mode = "force_installed";
          };
        } )
        extensions
      );
    };
  };

  policiesJson = pkgs.writeText "policies.json"
    (builtins.toJSON enterprisePolicies);

  mozillaCfg = pkgs.writeText "mozilla.cfg" ''
    // First line must be a comment

    // User customization
    ${extraPrefs}
  '';

  setupConfiguration = installDir: ''
    ${log "creating policies.json"}

    ${mkdir} -p "${installDir}/distribution"

    ${sponge} > "${installDir}/distribution/policies.json" \
           < ${policiesJson}

    ${log "preparing for autoconfig"}
    mkdir -p "${installDir}/defaults/pref"
    ${sponge} > "${installDir}/defaults/pref/autoconfig.js" <<EOF
      pref("general.config.filename", "mozilla.cfg");
      pref("general.config.obscure_value", 0);
    EOF

    ${log "writing mozilla.cfg"}
    ${sponge} > "${installDir}/mozilla.cfg" < ${mozillaCfg}
  '';

  configuration = pkgs.runCommand "firefox-configuration" {}
    (setupConfiguration "$out/lib/firefox");

# -----------------SYNCHRONISATION-----------------
  dataMap = import ./data-map.nix;

  expand = e: if builtins.hasAttr e dataMap
    then builtins.getAttr e dataMap
    else abort ''
      unknown keyword: ${e} should be one of:
      ${builtins.attrNames dataMap}
  '';

  persistentData = builtins.concatLists
    (builtins.map expand makePersistent)
    ++ extraPersistentData;

  readOnlyData = builtins.concatLists
    (builtins.map expand makeReadOnly)
    ++ extraReadOnlyData;

  allData = persistentData ++ readOnlyData;

  extensionIds = builtins.map (builtins.getAttr "id") extensions;
  extensionUrls = builtins.map (builtins.getAttr "url") extensions;

  filter-extensions = pkgs.callPackage ./filter-extensions {};

  syncProfile = data: ''
    for f in ${builtins.toString data}; do
      ${log "syncing $f"}
      if test -f "$sourcePath/$f" || test -d "$sourcePath/$f"
      then
          mkdir -p "$(dirname -- "$targetPath/$f")"
          ${cp} -RT "$sourcePath/$f" "$targetPath/$f"
      elif test -e "$sourcePath/$f"
      then
        ${log "warning: $sourcePath/$f is strange, not syncing."}
      fi
    done

  '' + lib.optionalString (extensionIds != []) ''

    mkdir -p "$targetPath/extensions"
    for id in ${builtins.toString extensionIds}; do
      if test -f "$sourcePath/extensions/$id.xpi"
      then
        ${log "syncing extension $id"}
        ${cp} -RT "$sourcePath/extensions/$id.xpi" \
               "$targetPath/extensions/$id.xpi"
      elif test -e "$sourcePath/extensions/$id.xpi"
      then
        ${log "warning: $sourcePath/extensions/$id.xpi is strange
               not syncing."}
      fi
    done

    if test -f "$sourcePath/extensions.json"
    then
      ${log "filtering extensions.json"}
      if ! ${filter-extensions}/bin/filter-extensions \
               ${builtins.toString extensionIds} \
               < "$sourcePath/extensions.json" \
               | ${sponge} "$targetPath/extensions.json"
      then
        ${err "filter-extensions failed, aborting."}
        exit 1
      fi
    fi
  '';

#-----------------PROFILE SETUP-----------------

  setupTmpHome = homePath: ''
    mkdir -p "${homePath}/.mozilla/firefox"

    if test -n "$HOME" && test -d "$HOME"
    then
      ${cp} "$HOME/.Xauthority" "${homePath}/.Xauthority"
    else
      ${err "$HOME does not exist or is not a directory, aborting."}
      exit 2
    fi

    ${sponge} > "${homePath}/.mozilla/firefox/profiles.ini" <<EOF
      [General]
      StartWithLastProfile=0

      [Profile0]
      Name=profile
      IsRelative=0
      Path=${ff-tmp-profile}
      Default=1
    EOF
  '';

# -----------------MAIN LOGIC-----------------

  ff-tmp-profile = "/tmp/firefox-profile";

  explicitProfile = ! (builtins.isNull profileLocation);
  persistentProfile = explicitProfile && (! readOnly);

  profilePath = "${profileLocation}/${profileName}";

  bashOpts = "set -efu -o pipefail";

  execFirefox =
    let wmClassArg = lib.optionalString (!builtins.isNull wmClass)
        "--class=${wmClass}";

        profileArg = "--profile ${ff-tmp-profile}";

        remoteArg = lib.optionalString (!allowRemoteCommands)
        "--no-remote";
  in ''
    ${pkgs.firefox}/bin/firefox \
      ${profileArg} ${remoteArg} ${wmClassArg} ${extraCliArgs} "$@"
  '';

  initProfile = ''
    if ! test -e ${profileLocation}
    then
      ${log "initializing profile at ${profileLocation}"}
      mkdir -p ${profileLocation}
    fi

    if test -e "${profilePath}" && ! test -L "${profilePath}"
    then
      ${err "your profile is a directory does not look right for a persistent setup, aborting"}
      exit 3
    fi
  '';

  collectGarbage = ''
    ${log "cleaning up ${profileLocation}"}

    find -- "${profileLocation}" -name '.profile_*' -print0 | while read -d $'\0' f
    do
      pid="$( ${basename} -- "$f" | ${sed} 's/.profile_\([^_]*\).*/\1/' )"
      ${log "found profile from $pid"}
      if ${ps} --quick-pid "$pid" > /dev/null
      then
        ${log "note: process $pid is still syncing"}
        ${lib.optionalString linearSync ''echo "waiting for it to finish.." && ${timeout} 5 ${tail} --pid "$pid" -f /dev/null''}
      else
        if snapshot="$(${readlink} -e ${profilePath})"
        then
          if test "$snapshot" != "$f"
          then
            ${log "garbage collecting $f"}
            ${rm} -Rf -- "$f" || true
          fi
        fi
      fi
    done
  '';

  syncBack = ''
    ${log "syncing back persistent data"}
    tmpProfilePath="${profileLocation}"/.profile_"$PROFILE_ID"_$(${uuidgen} --time)
    snapshot="$(${readlink} -m ${profilePath})"
    if test -e "$snapshot"
    then
      ${cp} --reflink=auto -RT -- "$snapshot" "$tmpProfilePath"
    fi
    snapshot2="$(${readlink} -m ${profilePath})"
    if test "$snapshot" = "$snapshot2"
    then
      sourcePath=${ff-tmp-profile}
      targetPath="$tmpProfilePath"
      ${syncProfile persistentData}
      ${ln} -srT -- "$tmpProfilePath" "${profilePath}.tmp-link" \
        && ${mv} -fT -- "${profilePath}.tmp-link" "${profilePath}"
    fi
  '';


  runtimeWrapper = pkgs.writeShellScriptBin "firefox-with"
  ( ''
      ${bashOpts}
      PROFILE_ID="$1"
      ${log "firefox-with runtimeWrapper running with PROFILE_ID $PROFILE_ID"}

      ${log "mounting tmpfs over /tmp for private storage"}
      ${mount} -t tmpfs tmpfs /tmp

      ${log "overlaying configuration over firefox runtime"}
      ${overlay} -o allow_other,auto_unmount,lowerdir=${configuration}:${firefox-unwrapped} ${firefox-unwrapped}
      mkdir -p ${ff-tmp-profile}
    ''

    + lib.optionalString explicitProfile ''
      ${log "seeding  temporary profile"}
      sourcePath=${profilePath}
      targetPath=${ff-tmp-profile}
      ${syncProfile allData}
    ''

    + ''
      ${log "mounting tmpfs over /root to make the shader cache happy"}
      # https://bugzilla.mozilla.org/show_bug.cgi?id=1535978
      ${mount} -t tmpfs tmpfs /root
    ''

    + lib.optionalString privateHome ''
        ${log "creating transient home directory"}
        ${setupTmpHome "/tmp/home"}
        export HOME=/tmp/home
      ''

    + ''
        ${log "executing firefox"}
        ${execFirefox}

        ${log "umounting overlay"}
        ${fusermount} -u ${firefox-unwrapped}
    ''

    + lib.optionalString persistentProfile syncBack
  );

  check-js = pkgs.callPackage ./check-js {};

in (pkgs.writeShellScriptBin exeName ''
    ${preSandbox}
    ${bashOpts}
    ${lib.optionalString persistentProfile initProfile}
    ${lib.optionalString persistentProfile collectGarbage}
    ${pkgs.utillinux}/bin/unshare --kill-child -r -m \
      ${runtimeWrapper}/bin/firefox-with "$BASHPID" "$@"
  '').overrideAttrs (oldAttrs: {
    buildCommand = oldAttrs.buildCommand
      + lib.optionalString checkJs ''
        ${check-js}/bin/check-js ${mozillaCfg}
      '';
    })
