{ pkgs ? import <nixpkgs> {}, haskellPackages ? pkgs.haskellPackages }:

pkgs.writers.writeHaskellBin "check-js" {
  libraries = with haskellPackages; [ language-javascript ];
  ghc = haskellPackages.ghc;
} (builtins.readFile ./check-js.hs)
