module Main where

import System.Exit
import qualified System.IO as IO
import qualified System.Environment as Env
import Language.JavaScript.Parser

main = do
  jsFile : _ <- Env.getArgs
  jsSource <- readFile jsFile
  case parse jsFile jsSource of
    Left err -> do
      IO.hPutStr IO.stderr
        $ "\n[check-js]:\n"
        <> jsFile <> " - parsing of source failed!\n"
        <> show err <> "\n\n"
      exitFailure
    Right _ -> do
      putStr
        $ "\n[check-js]:\n"
        <> jsFile <> " - source parses successfully.\n\n"
      exitSuccess

