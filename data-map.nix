{
  # See https://support.mozilla.org/en-US/kb/profiles-where-firefox-stores-user-data

  # the current/last session (e.g. open tabs)
  session = [
    "sessionstore.jsonlz4"
    "sessionCheckpoints.json"
    "sessionstore-backups"
  ];

  # you probably don't want to use this, it will interfere with the
  #  extensions option proper.
  extensions = [ "extensions" "extensions.json" ];

  extensions-data = [
    "browser-extension-data"
    "extension-settings.json"
    "extension-preferences.json"
    "addonStartup.json.lz4"
    "prefs.js" # *facepalm*
  ];

  # See https://developer.mozilla.org/en-US/docs/Web/API/IndexedDB_API/Browser_storage_limits_and_eviction_criteria
  managed-storage = [ "storage" "storage.sqlite" ];

  ui-data = [ "xulstore.json" ];
  site-prefs = [ "permissions.sqlite" "content-prefs.sqlite" ];
  cookies = [ "cookies.sqlite" ];
  dom-storage = [ "webappsstore.sqlite" "chromeappsstore.sqlite" ];
  certs = [ "cert9.db" ];
  file-handlers = [ "handlers.json" ];
  search-engines = [ "search.json.mozlz4" ];
  dictionary = [ "persdict.dat" ];
  mime = [ "handlers.json" ];
  history = [ "formhistory.sqlite" "places.sqlite" ];
  # This will not store the actual bookmarks (you need
  #  history for that), only their backups.
  bookmarks = [ "bookmarkbackups" "favicons.sqlite" ];
  passwords = [ "logins.json" "key4.db" ];
  about-config = [ "prefs.js" ];
  user-js = [ "user.js" ];
  security-module = [ "pkcs11.txt" ];
  time = [ "times.json" ];
}
