module Main where

import System.Exit
import qualified System.Environment as Env
import qualified System.IO as IO
import qualified Data.List as Lst
import qualified Data.Set as S

import Text.JSON
import Text.JSON.Types
import Text.JSON.Parsec (p_js_object)
import qualified Text.Parsec as PSec

abort e = IO.hPutStr IO.stderr e *> exitFailure

main = do
  ids <- Env.getArgs
  res <- PSec.parse p_js_object "stdin" <$> IO.getContents
  case res of
    Left err -> abort $ show err
    Right dat -> filterExtensions ids dat


filterExtensions :: [String] -> JSObject JSValue -> IO ()
filterExtensions ids dat =
  case get_field dat "addons" of
    Nothing -> abort "invalid data"
    Just (JSArray exts) -> putStr . flip showJSObject "" $
      set_field dat "addons"
      (JSArray . filter takeExt $ exts)
  where
    idSet = S.fromList ids

    takeExt :: JSValue -> Bool
    takeExt (JSObject jso) = case get_field jso "id" of
      Just (JSString id) -> S.member (fromJSString id) idSet
      _ -> True
    takeExt _ = True

