{ pkgs ? import <nixpkgs> {}, haskellPackages ? pkgs.haskellPackages }:

pkgs.writers.writeHaskellBin "filter-extensions" {
  libraries = with haskellPackages; [ json ];
  ghc = haskellPackages.ghc;
} (builtins.readFile ./filter-extensions.hs)

