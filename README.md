# firefox-with – a nix expression to launch firefox with well defined state

## What it is

This is a [nix](nix) expression which generates a
runtime wrapper for firefox.

The wrapper allows to specify in depth the initial state
of the firefox instance, that is:
* general configuration
* installed extensions

It also does some general isolation against the host system.
Note however that this is _not_ a sandboxing program (giving any
security guarantees). Any security benefits are entirely accidental
and should not be relied upon.
If you are looking for something like that, you may be interested
in [firejail](firejail) or running firefox in a virtual machine.


## Prerequisites

* the [nix](nix) package manager
* user namespaces support
* a recent firefox release (>=68) for managing extensions

## How to use

Example usage might look like this:

```nix
let 
  firefox-with = import (builtins.fetchGit {
    url = https://gitlab.com/xaverdh/firefox-with;
    rev = ...;
  });
  firefox = firefox-with {
    profileLocation = "/path/to/my/firefox/profile";
    profileName = "cryptic_hash.name";
    extensions = [
      { id = ...; url = ...; }
      { id = ...; url = ...; }
      { id = ...; url = ...; }
    ];
    makePersistent = [ "session" "extensions-data" ... ];
  };
in ...
```

## Documentation of options

### General configuration

* extraPolicies : `nix attribute set` (default = `{}`)

   A nix attribute set, describing the contents of the enterprise
   policies file (policies.json). Upstream docs can be
   found [here](enterprise-policies).
   The nix attribute set specified here is translated into json
   using ``builtins.toJSON``. E.g.
   ```nix
     extraPolicies = {
       SearchEngines.Add = [
         { Name = ...; URLTemplate = ...; Method = ...; }
       ];
     }
   ```
   will be translated into:
   ```json
   {
     "policies": {
       "SearchEngines": {
         "Add": {
           "Name" : ... ,
           "URLTemplate" : ... ,
           "Method" : ...
         }
       }
     }
   }
   ```

* extraPrefs : `string` (default = `""`)

   This is the content of the firefox ``.cfg`` AutoConfig file (a more
   powerful variant of user.js). Upstream docs can be
   found [here](auto-config).

* extraCliArgs : `string` (default = `""`)

   Extra arguments to inject into the command line, when
   executing firefox.

### Extensions

* extensions : `list of nix attribute sets` (default = `[]`)

   A list of extensions, that should be installed. An extension is
   specified by giving its [addon id](firefox-addon-id) and an
   url (a local path or remote), where the xpi file can be found.
   Note that local files are _not_ exempt from the signing
   requirement firefox imposes on extensions by default. So if your
   extension is not signed, consider setting
   ``xpinstall.signatures.required`` to ``false`` in ``extraPrefs``,
   i.e. :
   ```nix
     extraPrefs = ''
       pref("xpinstall.signatures.required",false);
     '';
   ```

   An example might look as follows:
   ```nix
     extensions = [
       { id = "{73a6fe31-595d-460b-a920-fcc0f8843232}"
         url = https://addons.mozilla.org/firefox/downloads/file/1189923/noscript_security_suite-latest-fx.xpi }
     ];
   ```

### Profile setup

* readOnly : `boolean` (default = `false`)

   When this is set to true, the profile will _not_ be written to.
   Its data will be used as a seed only, and no lock on the profile
   will be taken (overriding the options below).
   This allows several instances to share the same profile.

* profileLocation : `string or null` (default = `null`)

   Where the firefox profile is located (containing directory).
   If the value is null, a fresh empty profile will be created
   in a temporary location. By default, the profile given here
   will only be used as a seed. Only data you specify will be
   synced back to the profile.
   The following options control the details of this.

* profileName: `string` (default "profile")

   Name of your firefox profile folder (expected to be living in `profileLocation`).

* makeReadOnly : `list of keywords` (default = `[]`)

   Data from the original profile to make available read only in the
   temporary profile. For a list of available keywords, see
   the [translation map](data-map.nix).

* extraReadOnlyData : `list of relative paths in the profile directory` (default = `[]`)

   Files / directories from the original profile to make available
   read only in the temporary profile.

* makePersistent : `list of keywords` (default = `[]`)

   Data from the original profile to make available in a persistent
   fashion, i.e. they will be visible to the firefox process in the
   temporary profile and changes to these will be synced with the
   original profile. For a list of available keywords, see
   the [translation map](data-map.nix).

* extraPersistentData : `list of relative paths in the profile directory` (default = `[]`)

   Files / directories from the original profile to make available in
   a persistent fashion, i.e. they will be visible to the firefox
   process in the temporary profile and changes to these will be
   synced with the original profile.

### Misc

* exeName : `string` (default = "firefox")

  Executable name of the created wrapper. Useful to distinguish
  separate setups.

* privateHome : `boolean` (default = `true`)

   Whether to point HOME to a tmpfs mount.

* allowRemoteCommands : `boolean` (default = `false`)

   Whether to allow the firefox instance to send/receive remote
   commands from  other instances.

* wmClass : `string` (default = `"Firefox-With"`)

   Set this as the ``WM_CLASS``

* debug : `boolean` (default = `false`)

   Set this to true to get debug output on the console.

* checkJs : `boolean` (default = `true`)

  Whether to check the javascript code in extraPrefs for syntactic errors.

* preSandbox: `string` (default = `""`)

  Extra shell (read bash) commands to execute before entering the user
  namespace sandbox.

* fuse-overlayfs: `package` (default = `pkgs.fuse-overlayfs`)

  Allows overriding the fuse-overlayfs package used. May be useful to
  build with older nixpkgs, as we need version 0.7 at least.

## Legal
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

See [LICENSE](LICENSE) for more details.


[nix]: https://nixos.org/nix/

[firejail]: https://github.com/netblue30/firejail/

[auto-config]: https://support.mozilla.org/en-US/kb/customizing-firefox-using-autoconfig/

[enterprise-policies]: https://github.com/mozilla/policy-templates/blob/master/README.md

[firefox-addon-id]: https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/WebExtensions_and_the_Add-on_ID
